package org.guavaberry.game;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.glViewport;
import org.guavaberry.engine.graph.*;
import org.guavaberry.engine.IGameLogic;
import org.guavaberry.engine.Window;
import org.guavaberry.engine.GameObject;
import org.joml.Vector3f;

public class DummyGame implements IGameLogic {

    private GameObject[] gameObjects;
    private Window window;
    private final Renderer renderer;

    private float playerVelocity;
    private boolean moveUp;
    private boolean moveLeft;
    private boolean moveRight;
    private boolean moveDown;

    public DummyGame() {
        renderer = new Renderer();
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);

        moveUp = false;
        moveDown = false;
        moveLeft = false;
        moveRight = false;

        playerVelocity = 0.03f;

        Mesh meshPlayer = makeCube(0.1f);
        GameObject player = new GameObject(meshPlayer);
        player.setPosition(0.0f, 0.0f, -2f);

        Mesh meshGem = makeDiamond(0.06f);
        GameObject gem = new GameObject(meshGem);
        gem.setPosition(0.0f, 0.0f, -1f);

        gameObjects = new GameObject[] { player, gem };
    }

    @Override
    public void input(Window window) {
        // handle up keys
        if(window.isKeyPressed(GLFW_KEY_UP)){
            moveUp = true;
        }else{
            moveUp = false;
            //System.out.println("STOP");
        }

        // handle down keys
        if(window.isKeyPressed(GLFW_KEY_DOWN)){
            moveDown = true;
        }else{
            moveDown = false;
        }

        // handle left key
        if(window.isKeyPressed(GLFW_KEY_LEFT)){
            moveLeft = true;
        }else{
            moveLeft = false;
        }

        // handle up key
        if(window.isKeyPressed(GLFW_KEY_RIGHT)){
            moveRight = true;
        }else{
            moveRight = false;
        }
    }

    //update game state
    @Override
    public void update(float interval) {
        // MOVEMENT
        float xPos = gameObjects[0].getPosition().x;
        float yPos = gameObjects[0].getPosition().y;
        if (moveUp) {
            yPos += playerVelocity;
        }
        if (moveDown) {
            yPos -= playerVelocity;
        }
        if (moveLeft) {
            xPos -= playerVelocity;
        }
        if (moveRight) {
            xPos += playerVelocity;
        }
        gameObjects[0].setPosition(xPos, yPos, -2f);

        // ANIMATION
        //rotate cube
        float rotationYCube = gameObjects[0].getRotation().y + 2.5f;
        float rotationXCube = gameObjects[0].getRotation().x + 3.5f;
        float rotationZCube = gameObjects[0].getRotation().z + 4.5f;
        if ( rotationYCube > 360 ) {
           rotationYCube = 0;
        }
        if ( rotationXCube > 360 ) {
            rotationXCube = 0;
        }
        if ( rotationZCube > 360 ) {
            rotationZCube = 0;
        }
        Vector3f newRotCube = new Vector3f(rotationXCube, rotationYCube, rotationZCube);
        gameObjects[0].setRotation(newRotCube);

        // rotate diamond
        float rotationYDiamond = gameObjects[1].getRotation().y + 1.5f;
        if ( rotationYDiamond > 360 ) {
            rotationYDiamond = 0;
        }
        Vector3f newRotDiamond = new Vector3f(0.0f, rotationYDiamond, 0.0f);
        gameObjects[1].setRotation(newRotDiamond);
    }

    @Override
    public void render(Window window) {
        window.setClearColor(0.1f, 0.1f, 0.2f, 0.0f);
        renderer.render(window, gameObjects);
    }

    @Override
    public void cleanup () {
        renderer.cleanup();
    }

    public Mesh makeCube (float radius) {
        // Create the Mesh
        float[] positions = new float[]{
            -radius,  radius,  radius,
            -radius, -radius,  radius,
            radius, -radius,  radius,
            radius,  radius,  radius,
            -radius, radius, -radius,
            radius, radius, -radius,
            -radius, -radius, -radius,
            radius, -radius, -radius,
        };

        int[] indices = new int[]{
            0, 1, 3, 3, 1, 2,

            4, 0, 3, 5, 4, 3,

            6, 1, 0, 6, 0, 4,

            3, 2, 7, 5, 3, 7,

            2, 1, 6, 2, 6, 7,

            7, 6, 4, 7, 4, 5,
        };

        float[] colors = new float[]{
            //front face
            0.7f, 0.7f, 0.5f,
            0.7f, 0.7f, 0.5f,
            0.7f, 0.7f, 0.5f,
            0.7f, 0.7f, 0.5f,
            //top face
            1.0f, 0.7f, 0.0f,
            1.0f, 0.7f, 0.0f,
            1.0f, 0.7f, 0.0f,
            1.0f, 0.7f, 0.0f,
        };

        Mesh mesh = new Mesh(positions, indices, colors);
        return mesh;
    }

    public Mesh makeDiamond (float radius) {
        // Create the Mesh
        float[] positions = new float[]{
            // the square
            -radius, 0.0f, radius,
            radius, 0.0f, radius,
            -radius, 0.0f, -radius,
            radius, 0.0f, -radius,

            // upper peak
            0.0f, radius * 3f, 0.0f,
            0.0f, -radius * 3f, 0.0f,

        };

        int[] indices = new int[]{
            0, 4, 1,
            2, 4, 3,
            0, 4, 2,
            1, 4, 3,

            0, 5, 1,
            2, 5, 3,
            0, 5, 2,
            1, 5, 3,
        };

        float[] colors = new float[]{
            0.3f, 0.7f, 0.7f,
            0.3f, 0.7f, 0.7f,
            0.2f, 0.3f, 0.7f,
            0.3f, 0.5f, 0.7f,
            0.1f, 0.2f, 0.4f,
            0.1f, 0.2f, 0.4f,
        };

        Mesh mesh = new Mesh(positions, indices, colors);
        return mesh;
    }
}
