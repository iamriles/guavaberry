package org.guavaberry.game;
import org.guavaberry.engine.GameEngine;
import org.guavaberry.engine.IGameLogic;

public class Main {

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            IGameLogic gameLogic = new DummyGame();

            GameEngine gameEng =
                new GameEngine("WIP-DEMO", 600, 480, vSync, gameLogic);

            gameEng.start();

        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
    }
}
