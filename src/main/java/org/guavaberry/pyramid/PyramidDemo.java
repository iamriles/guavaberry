package org.guavaberry.pyramid;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.opengl.GL11.glViewport;
import org.guavaberry.engine.graph.*;
import org.guavaberry.engine.IGameLogic;
import org.guavaberry.engine.Window;
import org.guavaberry.engine.GameObject;
import org.joml.Vector3f;

public class PyramidDemo implements IGameLogic {

    private GameObject[] gameObjects;
    private Window window;
    private final Renderer renderer;

    public PyramidDemo() {
        renderer = new Renderer();
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);

        Mesh mesh = makePyramid(0.2f);
        GameObject pyramid = new GameObject(mesh);
        pyramid.setPosition(0.0f, 0.0f, -2f);
        gameObjects = new GameObject[] { pyramid };
    }

    @Override
    public void input(Window window) {
    }
//update game state
    @Override
    public void update(float interval) {

        float rotationy = gameObjects[0].getRotation().y + 1.5f;
        float rotationx = gameObjects[0].getRotation().x + 1.5f;
        float rotationz = gameObjects[0].getRotation().z + 1.5f;
        if ( rotationx > 360 ) {
            rotationx = 0;
        }
        if ( rotationy > 360 ) {
            rotationy = 0;
        }
        if ( rotationz > 360 ) {
            rotationz = 0;
        }
        Vector3f newRot = new Vector3f(rotationx, 0.0f, 0.0f);
        gameObjects[0].setRotation(newRot);
    }

    @Override
    public void render(Window window) {
        window.setClearColor(0.3f, 0.3f, 0.6f, 0.0f);
        renderer.render(window, gameObjects);
    }

    @Override
    public void cleanup () {
        renderer.cleanup();
    }

    public Mesh makePyramid (float radius) {
        // Create the Mesh
        float[] positions = new float[]{
            // the square
            -radius, 0.0f, radius,
            radius, 0.0f, radius,
            -radius, 0.0f, -radius,
            radius, 0.0f, -radius,

            // peak
            0.0f, radius * 1.5f, 0.0f,

        };

        int[] indices = new int[]{
            0, 1, 3,
            0, 2, 3,
            0, 4, 1,
            2, 4, 3,
            0, 4, 2,
            1, 4, 3,
        };

        float[] colors = new float[]{
            0.7f, 0.0f, 0.5f,
            0.7f, 0.0f, 0.5f,
            0.7f, 0.0f, 0.5f,
            0.7f, 0.0f, 0.5f,
            0.9f, 0.0f, 0.0f,
        };

        Mesh mesh = new Mesh(positions, indices, colors);
        return mesh;
    }
}
