package org.guavaberry.pyramid;
import org.guavaberry.engine.GameEngine;
import org.guavaberry.engine.IGameLogic;

public class Pyramid {

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            IGameLogic gameLogic = new PyramidDemo();

            GameEngine gameEng =
                new GameEngine("pyramid", 600, 480, vSync, gameLogic);

            gameEng.start();

        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
    }
}
