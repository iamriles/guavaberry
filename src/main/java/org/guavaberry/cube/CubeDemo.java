package org.guavaberry.cube;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.opengl.GL11.glViewport;
import org.guavaberry.engine.graph.*;
import org.guavaberry.engine.IGameLogic;
import org.guavaberry.engine.Window;
import org.guavaberry.engine.GameObject;
import org.joml.Vector3f;

public class CubeDemo implements IGameLogic {

    private GameObject[] gameObjects;
    private Window window;
    private final Renderer renderer;

    // background color stuff
    private float red;
    private float green;
    private float blue;

    public CubeDemo() {
        renderer = new Renderer();
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);
        Mesh mesh = makeCube(0.2f);
        GameObject cube = new GameObject(mesh);
        cube.setPosition(0.0f, 0.0f, -2f);
        gameObjects = new GameObject[] { cube };
    }

    @Override
    public void input(Window window) {
    }

    //update game state
    @Override
    public void update(float interval) {

        // background coloring
        if (red > 1.0) {
            red = 1.0f;
        } else {
            red += 0.002;
        }
        if (green > 1.0) {
            green = 1.0f;
        } else {
            green += 0.001;
        }
        if (blue > 1.0) {
            blue = 1.0f;
        } else {
            blue += 0.0005;
        }

        // you spin me right round...
        float rotationy = gameObjects[0].getRotation().y + 1.5f;
        float rotationx = gameObjects[0].getRotation().x + 1.5f;
        float rotationz = gameObjects[0].getRotation().z + 1.5f;
        if ( rotationx > 360 ) {
            rotationx = 0;
        }
        if ( rotationy > 360 ) {
            rotationy = 0;
        }
        if ( rotationz > 360 ) {
            rotationz = 0;
        }
        Vector3f newRot = new Vector3f(rotationx, rotationy, rotationz);
        gameObjects[0].setRotation(newRot);
    }

    @Override
    public void render(Window window) {
        window.setClearColor(red, green, blue, 0.0f);
        renderer.render(window, gameObjects);
    }

    @Override
    public void cleanup () {
        renderer.cleanup();
    }

    public Mesh makeCube (float radius) {
        // Create the Mesh
        float[] positions = new float[]{
            -radius,  radius,  radius,
            -radius, -radius,  radius,
            radius, -radius,  radius,
            radius,  radius,  radius,
            -radius, radius, -radius,
            radius, radius, -radius,
            -radius, -radius, -radius,
            radius, -radius, -radius,
        };

        int[] indices = new int[]{
            0, 1, 3, 3, 1, 2,

            4, 0, 3, 5, 4, 3,

            6, 1, 0, 6, 0, 4,

            3, 2, 7, 5, 3, 7,

            2, 1, 6, 2, 6, 7,

            7, 6, 4, 7, 4, 5,
        };

        float[] colors = new float[]{
            //front face
            0.7f, 0.5f, 0.1f,
            0.7f, 0.5f, 0.1f,
            0.7f, 0.5f, 0.1f,
            0.7f, 0.5f, 0.1f,
            //top face
            0.2f, 0.0f, 0.2f,
            0.2f, 0.0f, 0.2f,
            0.2f, 0.0f, 0.2f,
            0.2f, 0.0f, 0.2f,
        };

        Mesh mesh = new Mesh(positions, indices, colors);
        return mesh;
    }
}
