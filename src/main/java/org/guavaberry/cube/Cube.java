package org.guavaberry.cube;
import org.guavaberry.engine.GameEngine;
import org.guavaberry.engine.IGameLogic;

public class Cube {

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            IGameLogic gameLogic = new CubeDemo();

            GameEngine gameEng =
                new GameEngine("cube", 600, 480, vSync, gameLogic);

            gameEng.start();

        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
    }
}
