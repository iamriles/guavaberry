/**
 * Transformation Class
 * =================================
 * This is a sort of like Builder class for building:
 * the reason I say sorta is because its not making an
 * entirely new matrix just manipulating the data in ones that
 * already exist.
 *
 * Projection Matrices: gives depth to the OpenGL viewport (camera)
 * and handle window scaling.
 *
 * World Matrices: sort of like unit vectors and used to describe the
 * world space of the viewport.
 */

package org.guavaberry.engine.graph;

import org.joml.Vector3f;
import org.joml.Matrix4f;

public class Transformation {

    private Matrix4f projectionMatrix;
    private Matrix4f worldMatrix;

    /**
     * Initialize Matrices for building other matrices
     */
    public Transformation() {
        projectionMatrix = new Matrix4f();
        worldMatrix = new Matrix4f();
    }

    /**
     * Builds a projectionMatrix
     * @param fieldOfView fieldOfView of the "camera"
     * @param width the width of the "camera".
     * @param height the height of the "camera".
     * @param zNear the minimum depth of the "camera".
     * @param zFar the maximum depth of the "camera".
     * @return the finished projection matrix of the "camera".
     */
    public final Matrix4f buildProjectionMatrix (
                                                 float fieldOfView,
                                                 float width,
                                                 float height,
                                                 float zNear,
                                                 float zFar
                                                 ) {
        float aspectRatio = width / height;

        // reset the matrix to an identity
        // (an "empty" matrix)
        projectionMatrix.identity();

        // building the matrix
        projectionMatrix.perspective(
                                     fieldOfView,
                                     aspectRatio,
                                     zNear,
                                     zFar
                                     );

        return projectionMatrix;
    }


    /**
     * Builds World Matrix
     * @param the (x, y, z) offset to position a renderable object
     * @param rotation the rotation along the (x, y, z) axis's
     * @param scale how to scale a renderable object
     * @return the built matrix that let OpenGl know how to render an object in
     * world space
     */
    public Matrix4f buildWorldMatrix (
                                      Vector3f translation,
                                      Vector3f rotation,
                                      float scale
                                      ) {
        // reset the world Matrix
        worldMatrix.identity();

        // apply the translation offset to the matrix
        worldMatrix.translate(translation);

        // apply rotation offset to the matrix
        worldMatrix.rotateX((float)Math.toRadians(rotation.x));
        worldMatrix.rotateY((float)Math.toRadians(rotation.y));
        worldMatrix.rotateZ((float)Math.toRadians(rotation.z));

        // apply the scale to the matrix
        worldMatrix.scale(scale);

        return worldMatrix;
    }
}
