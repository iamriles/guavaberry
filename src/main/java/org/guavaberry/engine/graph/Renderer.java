package org.guavaberry.engine.graph;

import org.guavaberry.engine.Utils;
import org.guavaberry.engine.Window;
import org.guavaberry.engine.GameObject;

import org.lwjgl.system.MemoryUtil;
import org.joml.Matrix4f;
import org.joml.Math.*;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;


public class Renderer {

    private int vaoId;
    private int vboId;

    private ShaderProgram shaderProgram;
    private Transformation transformation;


    // projection constants
    private static final float ZNEAR = 0.01f;
    private static final float ZFAR = 1000.0f;
    private static final float FIELDOFVIEW =
        (float) Math.toRadians(60.0f);

    public Renderer() {
        transformation = new Transformation();
    }

    /**
     * finds and loads the shaders into the
     * ShaderProgram
     **/
    public void init(Window window) throws Exception {

        // set up shaders
        shaderProgram = new ShaderProgram();

        shaderProgram
            .createVertexShader(Utils.loadResource("shaders/vertex.glsl"));

        shaderProgram
            .createFragmentShader(Utils.loadResource("shaders/fragment.glsl"));

        float aspectRatio = (float) window.getWidth() / window.getHeight();

        shaderProgram.link();

        // link shader program first!
        shaderProgram.createUniform("projectionMatrix");
        shaderProgram.createUniform("worldMatrix");
    }

    /**
     * render the frame to the given GLFW window
     * @param window used to the renderer which window to render to
     * @param mesh vertex data that will be renderer
     **/
    public void render(Window window, GameObject[] gameObjects) {

        clear();

        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());
            window.setResized(false);
        }

        shaderProgram.bind();

        Matrix4f projectionMatrix =
            transformation.buildProjectionMatrix (
                                                  FIELDOFVIEW,
                                                  window.getWidth(),
                                                  window.getHeight(),
                                                  ZNEAR,
                                                  ZFAR
                                                  );

        shaderProgram.setUniform("projectionMatrix", projectionMatrix);

        for (GameObject gameObject : gameObjects){
            Matrix4f worldMatrix =
                transformation.buildWorldMatrix(
                                                gameObject.getPosition(),
                                                gameObject.getRotation(),
                                                gameObject.getScale()
                                                );

                shaderProgram.setUniform("worldMatrix", worldMatrix);
                gameObject.getMesh().render();
        }

        shaderProgram.unbind();
    }

    /**
     * This method frees the shaderProgram from memory
     * and does standard cleanup so the program will exit
     * as expected.
     **/
    public void cleanup() {
        if (shaderProgram != null) {
            shaderProgram.cleanup();
        }
    }

    /**
     * Calls OpenGl's clear to set the background color of the frame
     * to what ever value the GL_COLOR_BUFFER_BIT is set to.
     **/
    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}
