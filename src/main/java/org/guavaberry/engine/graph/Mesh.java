/**
 * Mesh Class
 * ==================================
 * This is a class to handle 3D models
 * by loading them into openGL and then
 * drawing them.
 */

package org.guavaberry.engine.graph;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import org.lwjgl.system.MemoryUtil;

public class Mesh {
    //Vertex array object
    private final int vaoId;

    //Vertex buffer objects
    private final int positionsVboId;
    private final int indicesVboId;
    private final int colorsVboId;

    private final int vertexCount;

    /**
     * Basic contructor for building simple 3D shapes out of
     */

    public Mesh(float[] vertexPositions, int[] vertexIndices, float[] vertexColors) {

        //setup buffers
        FloatBuffer positionsBuffer = null;
        IntBuffer indicesBuffer = null;
        FloatBuffer colorsBuffer = null;

        try {
            vertexCount = vertexIndices.length;

            vaoId = glGenVertexArrays();
            glBindVertexArray(vaoId);

            // POSITIONS BUFFER SETUP
            positionsVboId = glGenBuffers();
            positionsBuffer = MemoryUtil.memAllocFloat(vertexPositions.length);
            positionsBuffer.put(vertexPositions).flip();
            glBindBuffer(GL_ARRAY_BUFFER, positionsVboId);
            glBufferData(GL_ARRAY_BUFFER, positionsBuffer, GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

            // INDICES BUFFER SETUP
            indicesVboId = glGenBuffers();
            indicesBuffer = MemoryUtil.memAllocInt(vertexIndices.length);
            indicesBuffer.put(vertexIndices).flip();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVboId);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STATIC_DRAW);

            // COLOR BUFFER SETUP
            colorsVboId = glGenBuffers();
            colorsBuffer = MemoryUtil.memAllocFloat(vertexColors.length);
            colorsBuffer.put(vertexColors).flip();
            glBindBuffer(GL_ARRAY_BUFFER, colorsVboId);
            glBufferData(GL_ARRAY_BUFFER, colorsBuffer, GL_STATIC_DRAW);

            // first arguments is changed to 1 so that the colors are load in a different
            // "shader location"
            glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);

        } finally {
            // clean up memory used by the 
            if (positionsBuffer != null){
                MemoryUtil.memFree(positionsBuffer);
            }
            if (positionsBuffer != null){
                MemoryUtil.memFree(indicesBuffer);
            }
            if (colorsBuffer != null){
                MemoryUtil.memFree(colorsBuffer);
            }
        }
    }

    /**
     * This method binds all of the data related to the meshes and then
     * draws it.
     */

    public void render(){
        // bind the VAO
        glBindVertexArray(getVaoId());

        // Make sure the vertex arrays are good to go
        glEnableVertexAttribArray(0); // positions
        glEnableVertexAttribArray(1); // colors

        //actually draw the mesh
        glDrawElements(GL_TRIANGLES, getVertexCount(), GL_UNSIGNED_INT, 0);

        // restore state so that other things can be drawn if needed
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glBindVertexArray(0);
    }

    /**
     * getter for the Id of the Vertex Array
     * @return Id of the Vertex Array
     */
    public int getVaoId() {
        return vaoId;
    }

    /**
     * getter for the total number of verteces
     * @return number of verteces in the mesh
     */
    public int getVertexCount(){
        return vertexCount;
    }

    /**
     * cleans up the memory used by Opengl
     * that is related to the mesh.
     */
    public void cleanUp() {
        glDisableVertexAttribArray(0);

        // free up the Vertex buffer
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(positionsVboId);
        glDeleteBuffers(indicesVboId);
        glDeleteBuffers(colorsVboId);

        // free up the actual vertex array
        glBindVertexArray(0);
        glDeleteVertexArrays(vaoId);
    }

}//end of Mesh class
