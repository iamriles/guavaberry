package org.guavaberry.engine.graph;
import static org.lwjgl.opengl.GL20.*;

import java.util.*;
import java.nio.FloatBuffer;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.system.MemoryStack;
import org.joml.Matrix4f;


public class ShaderProgram {

    private final int programId;
    private final Map<String, Integer> uniforms;

    private int vertexShaderId;
    private int fragmentShaderId;


    /**
     * This will initialize our shaderProgram object with a programId
     * and will throw and exception if it cant.
     **/
    public ShaderProgram() throws Exception {

        //ask OpenGl to create a program Object and return its ID
        programId = glCreateProgram();
        System.out.println("[DEBUG]: programId: " + programId);

        uniforms = new HashMap<>();

        if (programId == 0){
            throw new Exception("[ERROR] Unable to Create shader");
        }
    }

    /**
     * This loads the "vertex shader" code into the ShaderProgram
     * and throws an exception if it cant.
     * @param shaderCode raw String of the shader code
     **/
    public void createVertexShader(String shaderCode) throws Exception {
        System.out.println("Creating Vertex Shader");
        vertexShaderId = createShader(shaderCode, GL_VERTEX_SHADER);
    }

    /**
     * This loads the " shader" code into the ShaderProgram
     * and throws an exception if it cant.
     * @param shaderCode raw String of the shader code
     **/
    public void createFragmentShader(String shaderCode) throws Exception {
        System.out.println("Creating fragment Shader");
        fragmentShaderId = createShader(shaderCode, GL_FRAGMENT_SHADER);
    }

    /**
     * This is a convenience method for creating shaders
     * if the shader cannot be created it will throw an exception.
     **/
    protected int createShader(String shaderCode, int shaderType) throws Exception {
        int shaderId = glCreateShader(shaderType);

        if (shaderId == 0){
            throw new Exception(
                                "[ERROR]: Could not create shader of type"
                                + shaderType
                                );
        }

        glShaderSource(shaderId, shaderCode);
        glCompileShader(shaderId);

        // check if the shader can be compiled
        if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0){
            throw new Exception(
                                "[ERROR]: Could not compile shader: " +
                                glGetShaderInfoLog(shaderId, 1024)
                                );
        }

        //attaches the shader to the shader program
        glAttachShader(programId, shaderId);

        return shaderId;
    }

    /**
     * This links the ShaderProgram to OpenGl
     **/
    public void link() throws Exception {
        glLinkProgram(programId);

        if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
            throw new Exception(
                                "[ERROR]: Could not link Shader code: " +
                                glGetShaderInfoLog(programId, 1024)
                                );
        }

        // Detach shaders
        if (vertexShaderId != 0) {
            glDetachShader(programId, vertexShaderId);
        }
        if (vertexShaderId != 0) {
            glDetachShader(programId, vertexShaderId);
        }


        //This is forDebugging
        glValidateProgram(programId);
        if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0){
            System.err.println(
                               "[WARNING]: Validating Shader cod: " +
                               glGetProgramInfoLog(programId, 1024)
                               );
        }
    }

    /**
     * This method sets up uniforms and obtains the new location
     **/
    public void createUniform(String uniformName) throws Exception {
        int uniformLocation =
            glGetUniformLocation(programId, uniformName);

        if (uniformLocation < 0) {
            throw new Exception(
                                "[ERROR]: new uniform found: "
                                + uniformName
                               );
        }

        uniforms.put(uniformName, uniformLocation);
    }

    public void setUniform(String uniformName, Matrix4f value) {
        // store the matrix into a float buffer
        try (MemoryStack stack = MemoryStack.stackPush()){
            FloatBuffer matrixBuffer = stack.mallocFloat(16);
            value.get(matrixBuffer);
            glUniformMatrix4fv(uniforms.get(uniformName), false, matrixBuffer);
        }
    }

    /**
     * This will bind the shader program to OpenGl
     **/
    public void bind() {
        glUseProgram(programId);
    }

    /**
     * This will unbind the shader program to OpenGl
     **/
    public void unbind() {
        glUseProgram(0);
    }

    /**
     * Clear the ShaderProgram
     **/
    public void cleanup() {
        unbind();
        if (programId != 0){
            glDeleteProgram(programId);
        }
    }
} // end of ShaderProgram class
