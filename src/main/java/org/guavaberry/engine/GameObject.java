/**
 * GameObject Class
 * ===================================
 * This is a Class for storing the basic
 * transformation data for the rendering
 * of a gameobject
 */

package org.guavaberry.engine;

import org.joml.Vector3f;
import org.guavaberry.engine.graph.Mesh;

public class GameObject {
    private final Mesh mesh;

    private Vector3f position;
    private Vector3f rotation;
    private float scale;

    /**
     * Mesh only constructor.
     * @param initMesh the Mesh that will me used for the game object.
     */
    public GameObject (Mesh initMesh) {
        mesh = initMesh;
        position = new Vector3f(0, 0, 0);
        rotation = new Vector3f(0, 0, 0);
        scale = 1.0f;
    }

    /**
     * Mesh only constructor.
     * @param initMesh the Mesh that will me used for the game object.
     * @param initPos the initial position of the GameObject.
     * @param initRot the initial rotation of the GameObject.
     * @param initScale the initial scale of the GameObject.
     */
    public GameObject (
                       Mesh initMesh,
                       Vector3f initPos,
                       Vector3f initRot,
                       Float initScale
                       ) {
        mesh = initMesh;
        position = initPos;
        rotation = initRot;
        scale = initScale;
    }

    // Setters
    /**
     * Sets the postion of the object.
     * @param newPos the "replacement" postion vector.
     */
    public void setPosition (Vector3f newPos){
        position = newPos;
    }
    public void setPosition (float x, float y, float z){
        position.x = x;
        position.y = y;
        position.z = z;
    }

    /**
     * Sets the rotation of the object.
     * @param newRot the replacement rotation vector.
     */
    public void setRotation (Vector3f newRot) {
        rotation = newRot;
    }

    /**
     * Sets the scale of the object.
     * @param newScale the replacement value for the scale.
     */
    public void setScale (float newScale) {
        scale = newScale;
    }

    //Getters
    /**
     * Gets the object's position
     * @return the objects position.
     */
    public Vector3f getPosition () {
        return position;
    }

    /**
     * Gets the object's rotation
     * @return the objects rotation.
     */
    public Vector3f getRotation () {
        return rotation;
    }

    /**
     * Gets the object's scale
     * @return the objects scale
     */
    public float getScale () {
        return scale;
    }

    /**
     * Gets the Mesh of the object
     * @return the objects Mesh
     */
    public Mesh getMesh () {
        return mesh;
    }
}
