package org.guavaberry.engine;

import java.util.Scanner;
import java.io.File;

public class Utils {
    // <NOTE> need to find a better way of doing this
    public static String loadResource(String fileName) throws Exception {        
        String result =
            new Scanner (new File(fileName))
            .useDelimiter("\\Z").next();
        return result;
    }
}
