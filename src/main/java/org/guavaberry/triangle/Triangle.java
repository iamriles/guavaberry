package org.guavaberry.triangle;
import org.guavaberry.engine.GameEngine;
import org.guavaberry.engine.IGameLogic;

public class Triangle {

    public static void main(String[] args) {
        try {
            boolean vSync = true;
            IGameLogic gameLogic = new TriangleDemo();

            GameEngine gameEng =
                new GameEngine("triangle", 600, 480, vSync, gameLogic);

            gameEng.start();

        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
    }
}
