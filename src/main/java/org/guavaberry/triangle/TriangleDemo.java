package org.guavaberry.triangle;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.opengl.GL11.glViewport;
import org.guavaberry.engine.graph.*;
import org.guavaberry.engine.IGameLogic;
import org.guavaberry.engine.Window;
import org.guavaberry.engine.GameObject;
import org.joml.Vector3f;

public class TriangleDemo implements IGameLogic {

    private GameObject[] gameObjects;
    private Window window;
    private final Renderer renderer;

    public TriangleDemo() {
        renderer = new Renderer();
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);

        // makes the triangle
        Mesh mesh = makeTriangle(0.5f);
        GameObject triangle = new GameObject(mesh);
        triangle.setPosition(0.0f, 0.0f, -2f);

        // load the triangle
        gameObjects = new GameObject[] { triangle };
    }

    @Override
    public void input(Window window) {
    }

    //update game state
    @Override
    public void update(float interval) {
        // spin
        float rotationy = gameObjects[0].getRotation().y + 1.5f;
        if ( rotationy > 360 ) {
            rotationy = 0;
        }
        Vector3f newRot = new Vector3f(0.0f, rotationy, 0.0f);
        gameObjects[0].setRotation(newRot);
    }

    @Override
    public void render(Window window) {
        window.setClearColor(0.0f, 0.2f, 0.3f, 0.0f);
        renderer.render(window, gameObjects);
    }

    @Override
    public void cleanup () {
        renderer.cleanup();
    }

    public Mesh makeTriangle (float radius){
        // Create the Mesh
        float[] positions = new float[]{
            radius, -(radius * 1.68f)/2 , 0.0f,
            -radius, -(radius * 1.68f)/2 , 0.0f,
            0.0f, (radius * 1.68f)/2, 0.0f,
        };

        int[] indices = new int[]{
            0, 1, 2,
        };

        float[] colors = new float[]{
            1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f,
        };

        Mesh mesh = new Mesh(positions, indices, colors);
        return mesh;
    }
}
