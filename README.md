<img align="Left" width="48px" src="icons/GuavaBerryIcon.png" />

# Guava Berry

## What is Guava Berry?

Guava Berry is a small hobbiest engine for building simple 3D games.
Its still a major work in progress though so not "game ready" yet.

## Todo
- ~~Window and GLFW setup~~
- ~~depth handling (you know so its actually "3D")~~
- implement basics of the ECS system
- implement linear transforms for model positioning
- .obj model loading
- texturing with UV's
- camera system to change the viewport


